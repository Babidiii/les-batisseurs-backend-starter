import path from "path";
import _ from "lodash";
import readFile from "../utils/readFile";

export function csvToJson(file) {
  try {
    const data = [];
    const lines = file.split(/\r?\n/);
    const ref = lines[0].split(";");
    let val;

    for (let i = 1; i < lines.length; i++) {
      val = Object.assign(
        {},
        ...lines[i].split(";").map((i, index) => ({
          [_.camelCase(ref[index])]: i,
        }))
      );

      data.push(val);
    }
    return data;
  } catch (e) {
    throw Error("Trying to parse cvs to json failed");
  }
}

export async function importBuildings() {
  const file = await readFile(
    path.join(__dirname, "../ressources/buildings.csv")
  );
  const data = csvToJson(file);
  return data.map((item) => {
    item.workers = [];
    return item;
  });
}

export async function importWorkers() {
  const file = await readFile(
    path.join(__dirname, "../ressources/workers.csv")
  );
  return csvToJson(file);
}
