import * as cardService from "./cardService";

describe("csvToJson", () => {
  test("transform a csv to a javascript object", async () => {
    const input = `\
id;reward
1;1
2;1`;
    const expected = [
      { id: "1", reward: "1" },
      { id: "2", reward: "1" },
    ];
    expect(cardService.csvToJson(input)).toEqual(
      expect.arrayContaining(expected)
    );
  });

  test("transform a cvs to a javascript object with empty cvs file", () => {
    expect(cardService.csvToJson(``)).toEqual([]);
  });
});
