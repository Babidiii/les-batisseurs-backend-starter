import * as gameService from "./gameService";
import * as constants from "../utils/gameConstant";
import { importWorkers, importBuildings } from "./cardService";
import uuid from "uuid/v4";
import readFile from "../utils/readFile";
import * as fakeData from "../test/fakeData/deck";

jest.mock("../utils/readFile.js");
jest.mock("./cardService");
jest.mock("constants");
jest.mock("uuid/v4");

jest.mock("fs", () => ({
  promises: {
    writeFile: jest.fn().mockResolvedValue(),
  },
}));

describe("gameConstant", () => {
  test("constant are correctly set", async () => {
    expect(constants.TOTAL_MONEY).toBeGreaterThanOrEqual(
      constants.MAX_PLAYER * constants.USER_START_MONEY
    );
  });
});

describe("importGame", () => {
  test("import json parsing is correct", async () => {
    const fileData = `\
  {
      "games":[]
  }`;
    const expected = { games: [] };

    readFile.mockResolvedValue(fileData);

    expect(await gameService.importGames()).toStrictEqual(expected);
  });
});

describe("createGame", () => {
  test("create a game", async () => {
    const fileData = { games: [] };

    // Mock the constants and functions used during the process
    gameService.importGames = jest.fn().mockResolvedValue(fileData);

    constants.NB_BUILD_VISIBLE = 2;
    constants.NB_WORK_VISIBLE = 2;
    constants.TOTAL_MONEY = 40;
    constants.USER_START_MONEY = 10;
    uuid.mockImplementation(() => "mon_id");
    importWorkers.mockResolvedValue(fakeData.workersValue);
    importBuildings.mockResolvedValue(fakeData.buildingsValue);
    Date.now = jest.spyOn(Date, "now").mockImplementation(() => 1607887587172);

    // The excpected result with the use of the constant/function mocked
    const expected = {
      id: "mon_id",
      currentPlayer: 1,
      moneyAvailable: 20,
      workers: [
        {
          id: "200",
          price: "2",
          stone: "0",
          wood: "0",
          knowledge: "1",
          tile: "1",
        },
        {
          id: "201",
          price: "2",
          stone: "0",
          wood: "1",
          knowledge: "0",
          tile: "1",
        },
      ],
      buildings: [
        {
          id: "100",
          reward: "0",
          victoryPoint: "2",
          stone: "0",
          wood: "2",
          knowledge: "1",
          tile: "0",
          stoneProduced: "3",
          woodProduced: "0",
          knowledgeProduced: "0",
          tileProduced: "0",
          workers: [],
        },
        {
          id: "101",
          reward: "0",
          victoryPoint: "1",
          stone: "0",
          wood: "1",
          knowledge: "0",
          tile: "1",
          stoneProduced: "2",
          woodProduced: "0",
          knowledgeProduced: "0",
          tileProduced: "0",
          workers: [],
        },
      ],
      remainingWorkers: 1,
      remainingBuildings: 1,
      nextWorker: {
        id: "202",
        price: "2",
        stone: "0",
        wood: "1",
        knowledge: "1",
        tile: "0",
      },
      nextBuilding: {
        id: "102",
        reward: "0",
        victoryPoint: "2",
        stone: "0",
        wood: "0",
        knowledge: "2",
        tile: "1",
        stoneProduced: "0",
        woodProduced: "3",
        knowledgeProduced: "0",
        tileProduced: "0",
        workers: [],
      },
      _private: {
        buildingDeck: [
          {
            id: "102",
            reward: "0",
            victoryPoint: "2",
            stone: "0",
            wood: "0",
            knowledge: "2",
            tile: "1",
            stoneProduced: "0",
            woodProduced: "3",
            knowledgeProduced: "0",
            tileProduced: "0",
            workers: [],
          },
        ],
        workerDeck: [
          {
            id: "202",
            knowledge: "1",
            price: "2",
            stone: "0",
            tile: "0",
            wood: "1",
          },
        ],
      },
      done: false,
      name: "Name test",
      createdDate: 1607887587172,
      players: [
        {
          id: 1,
          finishedBuildings: [],
          availableWorkers: [],
          underConstructionBuildings: [],
          money: 10,
          victoryPoints: 0,
          actions: 3,
        },
        {
          id: 2,
          finishedBuildings: [],
          availableWorkers: [],
          underConstructionBuildings: [],
          money: 10,
          victoryPoints: 0,
          actions: 3,
        },
      ],
    };

    expect(await gameService.createGames(2, "Name test", false)).toStrictEqual(
      expected
    );
  });
});

describe("getGame", () => {
  const GameData = `\
{
  "games": [
    {
      "id": "7728be1a-d0f0-4e70-88fd-8593e2f8c276",
      "currentPlayer": 1,
      "moneyAvailable": 20,
      "workers": [],
      "buildings": [],
      "remainingWorkers": 37,
      "remainingBuildings": 37,
      "nextWorker": {},
      "nextBuilding": {},
      "done": false,
      "name": "Name test",
      "createdDate": 1607887587172,
      "numberOfPlayers": 2,
      "players": null
    },
    {
      "id": "29a9f01a-006e-40b1-a5e8-19b96399fbe6",
      "currentPlayer": 2,
      "moneyAvailable": 20,
      "workers": [],
      "buildings": [],
      "remainingWorkers": 37,
      "remainingBuildings": 37,
      "nextWorker": {},
      "nextBuilding": {},
      "done": false,
      "name": "Name test",
      "createdDate": 1607887587172,
      "numberOfPlayers": 2,
      "players": null
    },
    {
      "id": "70834035-2afb-44ff-94db-199057e872f4",
      "currentPlayer": 1,
      "moneyAvailable": 10,
      "workers": [],
      "buildings": [],
      "remainingWorkers": 37,
      "remainingBuildings": 37,
      "nextWorker": {},
      "nextBuilding": {},
      "done": false,
      "name": "Name test",
      "createdDate": 1607887587172,
      "numberOfPlayers": 2,
      "players": null
    }
  ]
}`;

  test("find a game which exist by id", async () => {
    readFile.mockResolvedValue(GameData);

    const expected = {
      id: "29a9f01a-006e-40b1-a5e8-19b96399fbe6",
      currentPlayer: 2,
      moneyAvailable: 20,
      workers: [],
      buildings: [],
      remainingWorkers: 37,
      remainingBuildings: 37,
      nextWorker: {},
      nextBuilding: {},
      done: false,
      name: "Name test",
      createdDate: 1607887587172,
      numberOfPlayers: 2,
      players: null,
    };

    expect(
      await gameService.getGame("29a9f01a-006e-40b1-a5e8-19b96399fbe6")
    ).toStrictEqual(expected);
  });

  // test("doesn't find a game which exist by id", async () => {
  //   readFile.mockResolvedValue(GameData);

  //   expect(await gameService.getGame("id_qui_fait_foirer")).toThrow();
  // });
});
