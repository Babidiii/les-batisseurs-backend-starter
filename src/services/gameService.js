import path from "path";
import readFile from "../utils/readFile";
import { importBuildings, importWorkers } from "./cardService";
import { v4 as uuidv4 } from "uuid";
import * as gameConstant from "../utils/gameConstant";
import fs from "fs";
import _ from "lodash";

export async function importGames() {
  return readFile(path.join(__dirname, "../storage/games.json"))
    .then((data) => JSON.parse(data))
    .catch((_e) => {
      throw Error("Error while reading file");
    });
}

export async function createGames(numberOfPlayers, name, shuffle = true) {
  const gameData = await importGames();

  const shuffledWorkers = shuffle
    ? _.shuffle(await importWorkers())
    : await importWorkers();

  const shuffledBuildings = shuffle
    ? _.shuffle(await importBuildings())
    : await importBuildings();

  const game = {
    id: uuidv4(),
    currentPlayer: 1,
    moneyAvailable:
      gameConstant.TOTAL_MONEY -
      numberOfPlayers * gameConstant.USER_START_MONEY,
    workers: shuffledWorkers.splice(0, gameConstant.NB_WORK_VISIBLE),
    buildings: shuffledBuildings.splice(0, gameConstant.NB_BUILD_VISIBLE),
    remainingWorkers: shuffledWorkers.length,
    remainingBuildings: shuffledBuildings.length,
    nextWorker: shuffledWorkers.length !== 0 ? shuffledWorkers[0] : null,
    nextBuilding: shuffledBuildings.length !== 0 ? shuffledBuildings[0] : null,
    _private: {
      buildingDeck: shuffledBuildings,
      workerDeck: shuffledWorkers,
    },
    done: false,
    name,
    createdDate: Date.now(),
    players: new Array(numberOfPlayers).fill().map((e, i) => ({
      id: i + 1,
      finishedBuildings: [],
      availableWorkers: [],
      underConstructionBuildings: [],
      money: gameConstant.USER_START_MONEY,
      victoryPoints: 0,
      actions: gameConstant.MAX_ACTIONS,
    })),
  };

  gameData.games.push(game);
  await saveGame(gameData);
  return game;
}

export const filterGameData = (game) => {
  delete game._private;
  return game;
};

export async function saveGame(data) {
  const filename = path.join(__dirname, "../storage/games.json");

  fs.promises.writeFile(filename, JSON.stringify(data)).catch((e) => {
    throw Error("Error while saving game");
  });
}

export async function getGame(id) {
  const { games } = await importGames();
  const game = games.find((g) => g.id === id);

  if (!game) {
    throw Error("Game doesn't exist");
  }
  return game;
}
