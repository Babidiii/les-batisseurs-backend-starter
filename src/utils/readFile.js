import fs from "fs";

export default function readFile(filename) {
  return new Promise((resolve, reject) => {
    fs.promises.readFile(filename, "utf8").then(resolve).catch(reject);
  });
}
