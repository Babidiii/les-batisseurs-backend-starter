import HttpError from "../middlewares/HttpError";
const { validationResult } = require("express-validator");

export const checkIfParamAreValidated = (req) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new HttpError(422, "Validation failed, entered data is incorrect.");
  }
};
