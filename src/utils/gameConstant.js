export const TOTAL_MONEY = 40;
export const USER_START_MONEY = 10;
export const MAX_ACTIONS = 3;
export const NB_WORK_VISIBLE = 5; // number of worker cards visible on board
export const NB_BUILD_VISIBLE = 5; // number of building cards visible on board
export const MAX_PLAYER = 4;
export const MIN_PLAYER = 2;
