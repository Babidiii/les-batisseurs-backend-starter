import express from "express";
import healthRouter from "./healthRouter";
import cardRouter from "./cardRouter";
import gameRouter from "./gameRouter";

const router = express.Router();

router.use("/health", healthRouter);
router.use("/cards", cardRouter);
router.use("/games", gameRouter);

export default router;
