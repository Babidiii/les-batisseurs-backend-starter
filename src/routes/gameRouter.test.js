import request from "supertest";
import app from "../app";
import * as fakeGames from "../test/fakeData/games";
import * as gameService from "../services/gameService";

jest.mock("fs", () => ({
  promises: {
    writeFile: jest.fn().mockResolvedValue(),
  },
}));

describe("/games", () => {
  test("should response empty array to the get all games method", async () => {
    const fileData = { games: [] };

    gameService.importGames = jest.fn().mockResolvedValue(fileData);

    const response = await request(app).get("/games");
    expect(response.statusCode).toBe(200);
    expect(response.body).toStrictEqual([]);
  });

  test("should response games to the get all games method", async () => {
    const fileData = fakeGames.games;
    gameService.importGames = jest.fn().mockResolvedValue(fileData);

    const expected = [
      {
        id: "9af0960a-1cd4-4007-ade4-ad8fae784419",
        name: "Name test",
        done: false,
        createdDate: 1608066877281,
        players: [
          {
            id: 1,
            finishedBuildings: [],
            availableWorkers: [],
            underConstructionBuildings: [],
            money: 10,
            victoryPoints: 0,
            actions: 3,
          },
          {
            id: 2,
            finishedBuildings: [],
            availableWorkers: [],
            underConstructionBuildings: [],
            money: 10,
            victoryPoints: 0,
            actions: 3,
          },
        ],
        numberOfPlayers: 2,
      },
      {
        id: "my_id",
        name: "Name test",
        done: false,
        createdDate: 1608066878763,
        players: [
          {
            id: 1,
            finishedBuildings: [],
            availableWorkers: [],
            underConstructionBuildings: [],
            money: 10,
            victoryPoints: 0,
            actions: 3,
          },
          {
            id: 2,
            finishedBuildings: [],
            availableWorkers: [],
            underConstructionBuildings: [],
            money: 10,
            victoryPoints: 0,
            actions: 3,
          },
        ],
        numberOfPlayers: 2,
      },
    ];

    const response = await request(app).get("/games");
    expect(response.statusCode).toBe(200);
    expect(response.body).toStrictEqual(expected);
  });

  test("should response error to the get all games method", async () => {
    const fileData = null;

    gameService.importGames = jest.fn().mockResolvedValue(fileData);

    const response = await request(app).get("/games");
    expect(response.statusCode).toBe(404);
    expect(response.error.text).toBe("Games not found.");
    expect(response.body).toStrictEqual({});
  });

  test("should response an error to the get game by id method", async () => {
    const fileData = fakeGames.games;
    gameService.importGames = jest.fn().mockResolvedValue(fileData);

    const response = await request(app).get("/games/id_404").send();

    expect(response.statusCode).toBe(404);
    expect(response.error.text).toBe("Game not found.");
    expect(response.body).toStrictEqual({});
  });

  // test("should response to the get games by id method", async () => {
  //   const fileData = fakeGames.games;
  //   gameService.importGames.mockClear();
  //   gameService.importGames = jest.fn().mockResolvedValue(fileData);

  //   const expected = fakeGames.games.games[0];
  //   delete expected._private;

  //   // the await syntax for this test doesn't work for any reasons
  //   const response = request(app)
  //     .get("/games/9af0960a-1cd4-4007-ade4-ad8fae784419")
  //     .send();

  //   expect(response.statusCode).toBe(200);
  //   expect(response.body).toStrictEqual(expected);
  // });
});
