import express from "express";
import { body, param } from "express-validator";
import gameController from "../controllers/gameController";
import { MAX_PLAYER, MIN_PLAYER } from "../utils/gameConstant";

const router = express.Router();

router.post(
  "/",
  [
    body("numberOfPlayers").isInt({ min: MIN_PLAYER, max: MAX_PLAYER }),
    body("shuffle").default(true).isBoolean(),
    body("name").isString(),
  ],
  gameController.createGame
);

router.get("/", gameController.getAllGame);

router.get("/:gameId", param("gameId").isString(), gameController.getGame);

export default router;
