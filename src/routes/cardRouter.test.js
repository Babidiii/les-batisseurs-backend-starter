import request from "supertest";
import app from "../app";
import readFile from "../utils/readFile";

jest.mock("../utils/readFile");

describe("/cards/workers", () => {
  test("should response the GET method", async () => {
    const csvData = `\
id;price
1;1
2;1`;
    readFile.mockResolvedValue(csvData);
    const response = await request(app).get("/cards/workers");
    expect(response.statusCode).toBe(200);
    expect(response.body).toStrictEqual([
      { id: "1", price: "1" },
      { id: "2", price: "1" },
    ]);
  });

  test("should return 500 failed", async () => {
    readFile.mockRejectedValue(Error("Error while reading file"));
    const response = await request(app).get("/cards/workers");
    expect(response.statusCode).toBe(500);
    expect(response.text).toEqual("Can't read workers cards.");
  });
});

describe("/cards/buildings", () => {
  test("should response the GET method", async () => {
    const csvData = `\
id;reward
1;1
2;1`;
    readFile.mockResolvedValue(csvData);
    const response = await request(app).get("/cards/buildings");
    expect(response.statusCode).toBe(200);
    expect(response.body).toStrictEqual([
      { id: "1", reward: "1", workers: [] },
      { id: "2", reward: "1", workers: [] },
    ]);
  });

  test("should return 500 failed", async () => {
    readFile.mockRejectedValue(Error("Error while reading file"));
    const response = await request(app).get("/cards/buildings");
    expect(response.statusCode).toBe(500);
    expect(response.text).toEqual("Can't read buildings cards.");
  });
});
