import express from "express";
import "express-async-errors";
import bodyParser from "body-parser";
import morgan from "morgan";
import helmet from "helmet";
import cors from "cors";
import routes from "./routes";
import errorHandler from "./middlewares/errorHandler";

// App creation
const app = express();

// Middlewares
app.use(helmet());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "YOUR-DOMAIN.TLD"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use(cors());
app.use(morgan("short"));
app.use(bodyParser.json());
app.use(routes);
app.use(errorHandler);

export default app;
