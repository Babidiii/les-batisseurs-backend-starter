import HttpError from "../middlewares/HttpError";
import { checkIfParamAreValidated } from "../utils/errorCheck";

import {
  getGame,
  createGames,
  importGames,
  filterGameData,
} from "../services/gameService";

exports.createGame = async (req, res) => {
  checkIfParamAreValidated(req);
  try {
    const { numberOfPlayers, name, shuffle } = req.body;
    await createGames(numberOfPlayers, name, shuffle).then((game) => {
      res.status(201).json(filterGameData(game));
    });
  } catch (e) {
    throw new HttpError(500, "Can't create a game");
  }
};

exports.getGame = async (req, res) => {
  checkIfParamAreValidated(req);
  try {
    const game = await getGame(req.params.gameId);
    res.json(filterGameData(game));
  } catch (e) {
    throw new HttpError(404, "Game not found.");
  }
};

exports.getAllGame = async (req, res) => {
  try {
    const games = await importGames();
    res.json(
      games.games.map((g) => {
        const { id, name, done, createdDate, players } = g;
        return {
          id,
          name,
          done,
          createdDate,
          players,
          numberOfPlayers: players.length,
        };
      })
    );
  } catch (e) {
    throw new HttpError(404, "Games not found.");
  }
};
