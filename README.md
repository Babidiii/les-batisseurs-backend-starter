# Les Bâtisseurs - Moyen-Âge

## Install

- Run `yarn` or `yarn install` to install dependencies

## Start

- Run `yarn start` to start the app
- Open your browser on `http://localhost:3000/`

## Debug

- Run `yarn start:debug` to start the app in debug mode
- Press `F5` to attach the vscode debugger to the node process

## Test

- Run `yarn test` to start test suites
- Open `coverage/index.html` to see the tests code coverage information

## Linter/Code formatter

- Run `yarn lint` to see errors
- Run `yarn lint:fix` to automatically fix errors

## Commit

- For any commit refer to the documentation of commitlint in order to use the correct syntax.
  - **https://github.com/conventional-changelog/commitlint**
  - **https://commitlint.js.org/#/**

## Files structure

- **.vscode** Visual Studio Code project configuration
- **coverage** Tests code coverage information
- **node_modules** Dependencies of the application
- **src** Sources of the application
  - **ressources** Folder with cards as csv files
  - **routes** Routes of the application
  - **services** Services of the application
  - **app.js** Application creation
  - **index.js** Main of the application
- **.babelrc** Babel configuration file
- **.eslintrc** ESLint configuration file
- **.gitignore** List of files to exclude from git
- **jest.config.js** Jest configuration file
- **package-lock.json** Informations about dependencies automatically generated (do not modify by hand)
- **package.json** Project description
- **rules.pdf** Rules of Les Bâtisseurs - Moyen-Âge

# Documentation

**https://batisseurs-api.usson.me/**

- Erreur dans l'api pour la route GET/ /games pour findAllGames il manque la property schema :

```js
{
  id: String;
  name: String;
  numberOfPlayers: Number;
  done: Boolean;
  createdDate: String;
  players : player[] // manquant
}
```

## Validation

See **https://express-validator.github.io/docs/**

```

```
